package me.corriekay.taplist;

import static org.bukkit.Material.*;

import java.util.HashMap;
import java.util.HashSet;

import me.corriekay.taplist.utils.ExpUtil;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

public abstract class TapEngine implements Listener {
	
	private HashMap<String, Tap> taps = new HashMap<String, Tap>();
	private HashMap<ItemStack, TapDetails> finishedTaps = new HashMap<ItemStack, TapDetails>();
	
	public Integer preMortExp, postMortExp;
	public boolean reqWeapon, nonPlayerKills, transferExp, allowRecovery;
	private HashSet<Material> wepMats = new HashSet<Material>();
	
	public TapEngine(int pre, int post, boolean weapon, boolean nonPlayerKills, boolean allowRecovery, boolean transferExp) {
		preMortExp = pre;
		postMortExp = post;
		reqWeapon = weapon;
		this.nonPlayerKills = nonPlayerKills;
		this.transferExp = transferExp;
		this.allowRecovery = allowRecovery;
		addMats(DIAMOND_SWORD, IRON_SWORD, STONE_SWORD, WOOD_SWORD, BOW);
		Bukkit.getPluginManager().registerEvents(this, Main.getPlugin());
	}
	
	private void addMats(Material... mats) {
		for (Material mat : mats) {
			this.wepMats.add(mat);
		}
	}
	
	public boolean validateAttack(EntityDamageEvent e) {
		// System.out.println("Validating attack");
		if (e instanceof EntityDamageByEntityEvent) {
			EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e;
			Entity damaged, damager;
			damaged = event.getEntity();
			damager = event.getDamager();
			if (damaged instanceof Player) {
				Player pDamager;
				if (damager instanceof Player) {
					// System.out.println("Attack validating: damager is player");
					pDamager = (Player) damager;
				} else if (damager instanceof Projectile) {
					// System.out.println("Attack validating: damager is projectile");
					Projectile proj = (Projectile) damager;
					if (proj.getShooter() instanceof Player) {
						// System.out.println("Attack Validating: projectile shooter is player");
						pDamager = (Player) proj.getShooter();
					} else {
						return false;
					}
				} else {
					// System.out.println("Attack Validating: Failed. Not attacked by a player.");
					return false;
				}
				if (!(reqWeapon && wepMats.contains(pDamager.getItemInHand().getType()))) {
					// System.out.println("Attack Validating: failed: requires weapon, but no weapon equipped.");
					return false;
				} else {
					// System.out.println("Attack validating: Success. Player attacked player");
					return true;
				}
			}
		}
		// System.out.println("Attack validating: Failed.");
		return false;
	}
	
	@EventHandler
	public void onHit(EntityDamageByEntityEvent event) {
		if (validateAttack(event)) {
			Player damaged = (Player) event.getEntity(), damager;
			if (event.getDamager() instanceof Player) {
				damager = (Player) event.getDamager();
			} else if (event.getDamager() instanceof Projectile) {
				damager = (Player) ((Projectile) event.getEntity()).getShooter();
			} else {
				return;
			}
			onHit(damaged, damager, event);
			Tap t = taps.get(damaged.getName());
			if (t == null) {
				t = new Tap();
				t.targetName = damaged.getName();
				taps.put(damaged.getName(), t);
			}
			t.tapsSeconds.put(damager.getName(), preMortExp);
		}
	}
	
	public abstract void onHit(Player damaged, Player damager, EntityDamageByEntityEvent event);
	
	public void tickall() {
		for (String s : taps.keySet()) {
			taps.get(s).tick();
		}
		HashSet<ItemStack> removeme = new HashSet<ItemStack>();
		for (ItemStack s : finishedTaps.keySet()) {
			TapDetails td = finishedTaps.get(s);
			td.secondsLeft--;
			if (td.secondsLeft < 1) {
				removeme.add(s);
			}
		}
		for (ItemStack s : removeme) {
			// System.out.println("Removing an item stack for time has run out!");
			finishedTaps.remove(s);
		}
	}
	
	class Tap {
		String targetName;
		HashMap<String, Integer> tapsSeconds = new HashMap<String, Integer>();
		
		public boolean tick() {
			HashMap<String, Integer> newTaps = new HashMap<String, Integer>();
			for (String tapper : tapsSeconds.keySet()) {
				int seconds = tapsSeconds.get(tapper) - 1;
				if (seconds > 0) {
					newTaps.put(tapper, seconds);
				} else {
					removeFromSubTaps(targetName, tapper);
				}
			}
			subTapTick();
			if (newTaps.size() == 0) {
				return true;
			} else {
				tapsSeconds = newTaps;
				return false;
			}
		}
	}
	
	class TapDetails {
		ItemStack stack;
		HashSet<String> eligiblePlayers = new HashSet<String>();
		int secondsLeft;
		
		public TapDetails(ItemStack stack, int secondsToExpiration, String... eligiblePlayers) {
			this.stack = stack;
			secondsLeft = secondsToExpiration;
			for (String p : eligiblePlayers) {
				this.eligiblePlayers.add(p);
			}
		}
	}
	
	@EventHandler
	public void death(PlayerDeathEvent event) {
		// System.out.println("PDE: Player death event triggered.");
		EntityDamageEvent ede = event.getEntity().getLastDamageCause();
		boolean attackValidity = validateAttack(ede);
		if (!nonPlayerKills && !attackValidity) {
			return;
		}
		Entity killer = ((EntityDamageByEntityEvent) ede).getDamager();
		Player pKiller = null;
		if (killer instanceof Player) {
			pKiller = (Player) killer;
		} else {
			Projectile p = (Projectile) killer;
			pKiller = (Player) p.getShooter();
		}
		// System.out.println("Dropped Exp = " + event.getDroppedExp());
		if (transferExp) {
			ExpUtil.giveExperience(pKiller, event.getDroppedExp());
			event.setDroppedExp(0);
		}
		if (attackValidity) {
			// System.out.println("PDE: Attack valid");
			String[] players = finalizeTap(event, allowRecovery);
			if (players == null) {
				// System.out.println("PDE: Players null. Exiting");
				return;
			}
			// System.out.println("PDE: finalizing tap. Players on tap: " +
			// players.toString());
			for (ItemStack is : event.getDrops()) {
				TapDetails td = new TapDetails(is, postMortExp, players);
				finishedTaps.put(is, td);
			}
			taps.remove(event.getEntity().getName());
		}
	}
	
	public abstract String[] finalizeTap(PlayerDeathEvent event, boolean addPlayer);
	
	public abstract void subTapTick();
	
	public abstract void removeFromSubTaps(String damaged, String damager);
	
	@EventHandler
	public void onPickup(PlayerPickupItemEvent event) {
		// System.out.println("PIE: triggering");
		ItemStack is = event.getItem().getItemStack();
		TapDetails td = finishedTaps.get(is);
		if (td == null) {
			// System.out.println("PIE: No tap found for item. Allowing pickup");
			return;
		}
		// System.out.println("PIE: tap found for item stack. Checking for eligibility");
		Player player = event.getPlayer();
		if (td.eligiblePlayers.contains(player.getName())) {
			// System.out.println("PIE: Player eligible for tap, allowing pickup");
			finishedTaps.remove(is);
			return;
		}
		// System.out.println("PIE: Player not eligible for tap. Disallowing pickup");
		event.setCancelled(true);
		return;
	}
}
