package me.corriekay.taplist;

import java.io.File;
import java.io.IOException;

import me.corriekay.taplist.tapengines.DamageTotal;
import me.corriekay.taplist.tapengines.LastHit;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	private static Main plugin;
	private TapEngine tap;
	
	private FileConfiguration config;
	private int pre, post;
	private boolean reqWeapon, nonPlayerKills, allowRecovery, transferExp;
	
	public void onEnable() {
		plugin = this;
		getDataFolder().mkdirs();
		File configsFile = new File(getDataFolder(), "config.yml");
		if (!configsFile.exists()) {
			config = YamlConfiguration.loadConfiguration(getClassLoader().getResourceAsStream("config.yml"));
			try {
				config.save(configsFile);
			} catch (IOException e) {
				getLogger().severe("issue loading plugin! Self destructing...");
				e.printStackTrace();
				Bukkit.getPluginManager().disablePlugin(this);
				return;
			}
		} else {
			config = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "config.yml"));
		}
		post = config.getInt("postMortemTapExpiration", 12);
		pre = config.getInt("preMortemTapExpiration", 60);
		reqWeapon = config.getBoolean("requireWeapon", true);
		nonPlayerKills = config.getBoolean("invalidateTapOnNonPlayerKill", false);
		allowRecovery = config.getBoolean("allowKilledToRecoverItems", true);
		transferExp = config.getBoolean("transferExp", false);
		int tapType = config.getInt("tapType", 0);
		if (tapType == 0) {
			tap = new DamageTotal(pre, post, reqWeapon, nonPlayerKills, allowRecovery, transferExp);
		} else if (tapType == 1) {
			tap = new LastHit(pre, post, reqWeapon, nonPlayerKills, allowRecovery, transferExp);
		} else {
			getLogger().severe("INVALID TAP TYPE");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				tap.tickall();
			}
		}, 0, 20);
	}
	
	public static Main getPlugin() {
		return plugin;
	}
}
