package me.corriekay.taplist.tapengines;

import me.corriekay.taplist.TapEngine;

import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class LastHit extends TapEngine {
	
	public LastHit(int pre, int post, boolean weapon, boolean nonPlayerKills, boolean allowRecovery, boolean transferExp) {
		super(pre, post, weapon, nonPlayerKills, transferExp, allowRecovery);
	}
	
	@Override
	public void onHit(Player damaged, Player damager, EntityDamageByEntityEvent event) {
		return;
	}
	
	@Override
	public void subTapTick() {
		return;
	}
	
	@Override
	public void removeFromSubTaps(String damaged, String damager) {
		return;
	}
	
	@Override
	public String[] finalizeTap(PlayerDeathEvent event, boolean addPlayer) {
		Entity e = ((EntityDamageByEntityEvent) event.getEntity().getLastDamageCause()).getDamager();
		Player damager, damaged = event.getEntity();
		if (e instanceof Projectile) {
			damager = (Player) ((Projectile) e).getShooter();
		} else {
			damager = (Player) e;
		}
		if (addPlayer) {
			return new String[] { damager.getName(), damaged.getName() };
		} else {
			return new String[] { damager.getName() };
		}
	}
	
}
