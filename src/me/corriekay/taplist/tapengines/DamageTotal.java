package me.corriekay.taplist.tapengines;

import java.util.HashMap;

import me.corriekay.taplist.Main;
import me.corriekay.taplist.TapEngine;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class DamageTotal extends TapEngine {
	
	private HashMap<String, DamageTotals> taps = new HashMap<String, DamageTotals>();
	
	class DamageTotals {
		HashMap<String, Double> damageTotals = new HashMap<String, Double>();
		
		public void addDamage(Player player, double damage) {
			Double currentDamage = damageTotals.get(player.getName());
			if (currentDamage == null) {
				currentDamage = 0.0;
			}
			currentDamage += damage;
			damageTotals.put(player.getName(), currentDamage);
		}
		
		public void removePlayer(String player) {
			damageTotals.remove(player);
		}
	}
	
	public DamageTotal(int pre, int post, boolean reqWeapon, boolean nonPlayerKills, boolean allowRecovery, boolean transferExp) {
		super(pre, post, reqWeapon, nonPlayerKills, transferExp, allowRecovery);
	}
	
	@Override
	public void onHit(Player damaged, Player damager, EntityDamageByEntityEvent event) {
		DamageTotals dt = taps.get(damaged.getName());
		if (dt == null) {
			dt = new DamageTotals();
			taps.put(damaged.getName(), dt);
		}
		dt.addDamage(damager, event.getDamage());
	}
	
	@Override
	public void subTapTick() {
		
	}
	
	@Override
	public void removeFromSubTaps(String damaged, String damager) {
		DamageTotals dt = taps.get(damaged);
		if (dt == null) {
			Main.getPlugin().getLogger().warning("Tap missing for player " + damaged + ".");
			return;
		}
		dt.removePlayer(damager);
	}
	
	@Override
	public String[] finalizeTap(PlayerDeathEvent event, boolean addPlayer) {
		DamageTotals dt = taps.get(event.getEntity().getName());
		if (dt == null) {
			Main.getPlugin().getLogger().warning("Tap missing for player " + event.getEntity().getName() + ".");
			return null;
		}
		String damaged = event.getEntity().getName();
		String tapper = null;
		double damage = 0.0;
		for (String s : dt.damageTotals.keySet()) {
			double ddamage = dt.damageTotals.get(s);
			if (ddamage > damage) {
				damage = ddamage;
				tapper = s;
			}
		}
		if (addPlayer) {
			return new String[] { tapper, damaged };
		} else {
			return new String[] { tapper };
		}
	}
	
}
